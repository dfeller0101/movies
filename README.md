## Movies

## Overview
This was an interesting project to do some programming in PHP for the 1st time.  

## REST Services
1. List of movies by title, rating, and categor: http://<host>:3000/movies/list?title=youth&rating=pg&category=family
2. List of details for movies: http://<host>:3000/movies/details?title=youth
3. List of actors for movies: http://<host>:3000/movies/actors?title=youth


## Running this code


This project requires docker.  Can be issues with Win 10 and docker, ended up having success with Azure / linux.

Launch code with these commands
cd developer_test_server/
docker-compose up -d


